# compose:examples/v2
## @edt ASIX-M05 Curs 2021-2022


### Treballar amb docker compose


**web21 / Net21 / Portainer**

```
$ docker-compose -f docker-compose-v2-web21-net21-portainer.yml up -d

$ docker-compose -f docker-compose-v2-web21-net21-portainer.yml ps

$ docker-compose -f docker-compose-v2-web21-net21-portainer.yml top

$ docker-compose -f docker-compose-v2-web21-net21-portainer.yml down

```

**ldap + phplapadmin**

Atenció: el desplegament de ldap+phpldapadmin té mal configurat el nomd e host. És un execici 
interessant un cop desplegat entrar el container phpldapadmin i editar el fitxer de condifuració 
*config.php* i modificar el nom del servidor ldap al que ha de contactar per *ldap.edt.org*.
Un cop fet cal reinicialitzar el servei web (apache) però sense matar el container. Es pot fer
amb *kill -1 PID* però cal saber el PID de l'apache. Per fer-ho potser cal instal·lar procps i 
poder fer l'ordre *ps ax*.


**postgres + adminer**

```
$ docker-compose -f training.yml up -d
Creating network "postgres_default" with the default driver
Creating postgres_db_1      ... done
Creating postgres_adminer_1 ... done


# Connectar a http://ip:8080
# Use postgres/example user/password credentials
# System: PostgreSQL (atenció!)
# Server: db
# Username: postgres
# Password: passwd
# Database: training

$ docker-compose -f training.yml down
Stopping postgres_adminer_1 ... done
Stopping postgres_db_1      ... done
Removing postgres_adminer_1 ... done
Removing postgres_db_1      ... done
Removing network postgres_default
```

