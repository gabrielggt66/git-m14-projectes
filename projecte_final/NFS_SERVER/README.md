# PAM22:LDAP

Configura el container PAM (--privileged) per:

- Permetre l’autenticació local dels usuaris unix locals (unix01, unix02 i unix03)
- Permetre l’autenticació d’usuaris de LDAP.
Per fer-ho s’utilitzarà la imatge ldap22/grup (o ldap22:latest) que conté el llistat final
d’usuaris i grups LDAP amb els que treballar.
- Als usuaris LDAP se’ls ha de crear el directori home automàticament si no existeix.
- Als usuaris LDAP se’ls ha de crear un recurs temporal, dins del home anomenat tmp.
Un ramdisk de tipus tmpfs de 100M.
- Tots els usuaris han de poder modificar-se el password. Els usuaris locals i també
els usuaris de LDAP.

*Atenció: potser cal refer la configuració ACL de la base de dades LDAP edt.org per permetre que els usuaris es puguin modificar el seu password.*

### Crear imatge

```
docker build -t marleneflor/pam22:ldap .
```

### Engegar container

```
docker run --rm --name pam.edt.org -h pam.edt.prg --net 2hisx --privileged -it marleneflor/pam22:ldap
```

> Comprovar connectivitat entre ldap container i pam container. Desde el container pam ...

```
nmap ldap.edt.org
```
```
Starting Nmap 7.80 ( https://nmap.org ) at 2022-11-16 08:03 UTC
Nmap scan report for ldap.edt.org (172.18.0.2)
Host is up (0.000012s latency).
rDNS record for 172.18.0.2: ldap.edt.org.2hisx
Not shown: 999 closed ports
PORT    STATE SERVICE
389/tcp open  ldap
MAC Address: 02:42:AC:12:00:02 (Unknown)
```

> Instal·lar packets per fer l'autenticació ldap

```
apt-get install libnss-ldap libpam-ldap nslcd
``` 

> Configurar el client ldap en el /etc/ldap/ldap.conf les opcions per defecte perque es connecti al ldap.edt.org

```
BASE	dc=edt,dc=org
URI	ldap://ldap.edt.org
```

> Verificar amb el ldapsearch que està ben configurat el client

```
ldapsearch -x -LLL dn
```

> Configuració del /etc/nsswitch.conf per permetre la recerca en ldap

```
passwd:         files ldap
group:          files ldap
```

> Configurar el nslcd (local LDAP name service daemon), el fitxer /etc/nslcd.conf. El /etc/nscd el deixem igual
> El nslcd s'encarrega de fer les consultes al serever ldap i el nscd s'encarrega d'emmagatzemar els resultats

```
# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.edt.org 

# The search base that will be used for all queries.
base dc=edt,dc=org
```

> Engegar serveis que son els clients que faran les preguntes i guardaràn les respuestes al ldap

```
/usr/sbin/nslcd 
/usr/sbin/nscd 
```

> Verificar la resolucio de qualsevol servei que estaba al nsswitch

```
getent passwd
getent group
```

> **ELS COMMON ES MODIFIQUEN AUTOMÀTICAMENT PER AFEGIR EL *PAM_LDAP.SO*  QUAN DESCARREGUEM ELS PAQUETS DE *LIBPAM-LDAP*, PER TANT EN EL DOCKERFILE NOMÉS HEM DE POSSAR EL *COMMON-SESSION* QUE ÉS ON AFEGIM EL *PAM_MKDIE.SO***

> Configurar el PAM. Modificar o mirar els common per verificar que sigui el pam_ldap.so

- /etc/pam.d/common-auth
```
auth	[success=2 default=ignore]	pam_unix.so nullok
auth	[success=1 default=ignore]	pam_ldap.so use_first_pass
```
- /etc/pam.d/common-session
```
session	required	pam_unix.so 
session	optional	pam_mount.so 
session	optional			pam_ldap.so 
```
- /etc/pam.d/common-password
```
password	[success=2 default=ignore]	pam_unix.so obscure use_authtok try_first_pass yescrypt
password	[success=1 user_unknown=ignore default=die]	pam_ldap.so use_authtok try_first_pass
```
- /etc/pam.d/common-account
```
account	[success=2 new_authtok_reqd=done default=ignore]	pam_unix.so 
account	[success=1 default=ignore]	pam_ldap.so 
```

> Editar el common-session per afegir el pam_mkhomedir.so

```
session	required	pam_unix.so
session optional 	pam_mkhomedir.so 
session	optional	pam_mount.so 
session	optional	pam_ldap.so 
```

> Editar el /etc/security/pam_mount.conf.xml per afegir el directori tmp

```
<volume
	user="*"
	fstype="tmpfs"
	options="size=100M"
	mountpoint="~/tmp"
/>
```


