
# debian base
## @edt  M06-ASIX

#### debian basic 2022
* **debian basic** Imatge debian amb els paquets basics instalats

### Dockerfile

Selecionem la imatge de debian
```
FROM debian:latest
```
Instalem els paquets basics
```
RUN apt-get update && apt-get -y install procps iproute2 iputils-ping nmap tree
```
Crrem el directori /opt/docker
```
WORKDIR /tmp 
RUN mkdir /opt/docker
```
