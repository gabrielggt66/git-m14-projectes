# AWS22 base
## @edt  M14-ASIX

#### AWS 2022
* **ldap22:arg** Servidor ldap basic amb base de dades edt.org. amb persistencia de dades mitjançant un fitxer creat el primer cop de muntar la base de dades.

* **ldap_pam_phpldapadmin.yml** Fitxer compose que conte inicia les  imatges de ldap22:arg i phpldapadmin:base,crea els volums i el network 2HISX.
