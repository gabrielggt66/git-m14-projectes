### REPTE 5

## SSH 
```
ssh -i ansible_key ansible@192.168.56.10

ssh-keygen -f "/home/astro/.ssh/known_hosts" -R "192.168.56.10"
```
# PLAYBOOK
```
ansible-playbook -u ansible --private-key ansible_key -i inventory_lab_public.yaml playbook.yaml

```
## Para una tarea especifica: 
```
ansible-playbook -u ansible --private-key ansible_key -i inventory_lab_public.yaml --start-at-task="task_name" playbook.yaml
```

## TEST

```
localhost: 
ldapsearch -x -LLL -b 'dc=edt,dc=org'
ldapsearch -x -LLL -b 'dc=edt,dc=org' cn

browser: 
http://192.168.56.10/phpldapadmin
cn=Manager,dc=edt,dc=org
passwd:secret
```
