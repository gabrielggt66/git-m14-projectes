# ldap22 base
## @edt  M06-ASIX

#### ldapserver 2022
* **ldap22:base** Servidor ldap basic amb base de dades edt.org. Aquesta imatge engega amb CMD un script anomenat startup.sh amb les seguents instrucions:




Esborrar els directoris de configuració i de dades
```
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
```
Generar el directori de configuració  slapd.d amb el  fitxer de configuració slapd.conf del gitlab
```
slaptest -f slapd.conf -F /etc/ldap/slapd.d
```
Injectem a baix nivell les dades de la BD 
```
slapadd -F /etc/ldap/slapd.d -l edt-org.ldif
```
Cambiem la propietat del directori de dades i de configuració a l'usuari openldap
```
chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
```
Per a executar el servei de slapd li posem -d0 per a que ho fagui en foreground
```
/usr/sbin/slapd -d0
```
### Dockerfile

Descarreguem una imatge basada en Debian
```
FROM debian:latest
```
Posem les etiquetes
```
LABEL author="@gabrielggt66"
LABEL subject="ldapserver 2022"
```
Actualitzem i instalem els paquets necesaris per al servei slapd i altres paquets per a poder fer accions interactivament. 
```
RUN apt-get update
ARG DEBIAN_FRONTEND=non_interactive
RUN apt-get install -y procps iproute2 tree nmap iputils-ping slapd ldap-utils
```
Creem el directori /opt/docker i copiem tot el directori actiu dins del directori del container
```
RUN mkdir /opt/docker
WORKDIR /opt/docker
COPY * /opt/docker/
```
Fem que el progrma startup.sh sigui un executable
```
RUN chmod +x /opt/docker/startup.sh 
```
Activem el script startup.sh al iniciar el container
```
CMD /opt/docker/startup.sh
```
Exposem el port 389 del servei ldap. 
```
EXPOSE 389
```

### Ordres de docker

Carreguem les dades del Dockerfile i construim la imatge
```
docker build -t kevin16gonzalez/ldap22:base .
```
Amb l'imatge creem el container del servei ldap propagant el port 389 i engegantlo en mode detach
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -d kevin16gonzalez/ldap22:base

```
