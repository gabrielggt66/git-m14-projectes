# ldap22 var
## @edt  M06-ASIX

#### ldapserver 2022
* **ldap22:var** Servidor ldap amb varaibles d'entorn que cambien els valors del administrador  del servei ldap i la contraseña

## Ordres per engegar el container

Dins l'rdre d'arrancada especifiquem les variables amb l'opcio -e
```
docker run --rm -e ADMIN=Juan -e PASSWD=jupiter --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389  -d kevin16gonzalez/ldap22:var 
```

## Configuracio startup.sh

Al fitxer startup.sh li afegim dos ordres sed per substituir el manager i el passwd  a l'arrencada del container.
```
sed  -i "/^rootdn/ s/Manager/$ADMIN/" slapd.conf
sed  -i "/^rootpw/ s/secret/$PASSWD/" slapd.conf 
```

## Comprovacio

Per comprovar que ha anat be executem una ordre com administrador normal.En aquest cas tindria que donar error.

```
ldapsearch -x -LLL -D "cn=Manager,dc=edt,dc=org" -w secret -b "dc=edt,dc=org" | grep dn
ldap_bind: Invalid credentials (49)

```
Ara provem amb les variables que hem introduit
```
ldapsearch -x -LLL -D "cn=juan,dc=edt,dc=org" -w jupiter -b "dc=edt,dc=org" | grep dn
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Admin System,ou=usuaris,dc=edt,dc=org

```



