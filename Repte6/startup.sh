#!/bin/bash
# @edt ASIX M14-PROJECTE
# Descripció:Programa per a ldap que segons ,les variables que creem en la contrucio del container,
#Cambia els valors del administrador i la seva contraseña
echo "Inicialitzacio BD ldap edt.org"
sed  -i "/^rootdn/ s/Manager/$ADMIN/" slapd.conf
sed  -i "/^rootpw/ s/secret/$PASSWD/" slapd.conf 
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
/usr/sbin/slapd -d0
