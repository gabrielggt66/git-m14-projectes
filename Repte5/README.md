# ldap22 start
## @edt  M06-ASIX

#### ldapserver 2022
* **ldap22:start** Servidor ldap que executa fa diferents accions a la bases de dades depenent de l'argument donat a l'entrypoint
 

* initdb → ho inicialitza tot de nou i fa el populate de edt.org.


* slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades.

* start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents.


* slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada


## ENTRYPOINT 

Modificar el Dockerfile i cambiar CMD per ENTRYPOINT

```
ENTRYPOINT /opt/docker/startup.sh
```

## Initdb




Modificar el document startup
```
case $1 in
	"initdb")
		rm -rf /var/lib/ldap/*
		rm -rf /etc/ldap/slapd.d/*
		slaptest -f slapd.conf -F /etc/ldap/slapd.d
		slapadd -F /etc/ldap/slapd.d -l edt-org.ldif
		chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
		/usr/sbin/slapd -d0;;

```
Engegar el servei ldap amb l'argument initdb 

``` 
 docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-conf:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d kevin16gonzalez/ldap22:start initdb
```
Comprobar el contingut de la base de dades

```
ldapsearch -x -LLL -b 'dc=edt,dc=org' | grep dn 
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Admin System,ou=usuaris,dc=edt,dc=org

```

## Slapd

Modificar el document startup
```
	"slapd")
		rm -rf /var/lib/ldap/*
		rm -rf /etc/ldap/slapd.d/*
		slaptest -f slapd.conf -F /etc/ldap/slapd.d
		chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
		/usr/sbin/slapd -d0;;

```
Engegar el servei ldap amb l'argument slapd 

``` 
 docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-conf:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d kevin16gonzalez/ldap22:start slapd
```
Comprobar el contingut de la base de dades
```
docker exec -it ldap.edt.org slapcat
dn: dc=nodomain
objectClass: top
objectClass: dcObject
objectClass: organization
o: nodomain
dc: nodomain
structuralObjectClass: organization
entryUUID: af2ba5d0-d8df-103c-9700-6d62c7d8b7af
creatorsName: cn=admin,dc=nodomain
createTimestamp: 20221005095610Z
entryCSN: 20221005095610.581865Z#000000#000#000000
modifiersName: cn=admin,dc=nodomain
modifyTimestamp: 20221005095610Z

```

## Slapcat
Modificar el document startup

```
	"slapcat")
		if [ $# -eq 2 ] 
		then
			if [ $2 = "n1" ]
			then
				slapcat -n1 
			else
				slapcat -n0 
			fi
		else
			slapcat 
		fi;;
```

Engegar el servei ldap amb el argument slapcat selecionat i posant el mode interactiu per a que es mostri per pantalla

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-conf:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it kevin16gonzalez/ldap22:start slapcat
```

Aquesta seria la reacio
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-conf:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it kevin16gonzalez/ldap22:start slapcat
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org
structuralObjectClass: organization
entryUUID: 1c6a9556-df1c-103c-9249-cf4c56ea0922
creatorsName: cn=Manager,dc=edt,dc=org
createTimestamp: 20221013082350Z
entryCSN: 20221013082350.646424Z#000000#000#000000
modifiersName: cn=Manager,dc=edt,dc=org
modifyTimestamp: 20221013082350Z

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectClass: organizationalunit
structuralObjectClass: organizationalUnit
entryUUID: 1c6b0b26-df1c-103c-924a-cf4c56ea0922
creatorsName: cn=Manager,dc=edt,dc=org
createTimestamp: 20221013082350Z
entryCSN: 20221013082350.649488Z#000000#000#000000
modifiersName: cn=Manager,dc=edt,dc=org
modifyTimestamp: 20221013082350Z

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectClass: organizationalunit
structuralObjectClass: organizationalUnit
entryUUID: 1c6c2894-df1c-103c-924b-cf4c56ea0922
creatorsName: cn=Manager,dc=edt,dc=org
createTimestamp: 20221013082350Z
entryCSN: 20221013082350.656795Z#000000#000#000000
modifiersName: cn=Manager,dc=edt,dc=org
modifyTimestamp: 20221013082350Z
...
...
```
## Start / edtorg / res

Modificar el document startup(en aquest cas nomes cal posar l'ordre per a engergar el servei ldap en detach)
```
	"start"|"edtorg"|"")
		/usr/sbin/slapd -d0;;
```

Engegar el servei ldap amb un dels tres arguments selecionats 

``` 
 docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-conf:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d kevin16gonzalez/ldap22:start start
```
Per comprobar la persistencia de dades fem un llistat del usuaris del sistema
```
ldapsearch -x -LLL -b 'dc=edt,dc=org' | grep dn 
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Admin System,ou=usuaris,dc=edt,dc=org

```
Eliminem a un usuari amb la seguent ordre
```
ldapdelete -x -D "cn=Manager,dc=edt,dc=org" -w secret "cn=Pere Pou,ou=usuaris,dc=edt,dc=org"
```

Apaguem el container i el tornem a engegar

```
docker stop ldap.edt.org
```
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-conf:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d kevin16gonzalez/ldap22:start start
```
Llistem de nou els usuaris i observem que no esta el usuari que hem eliminat abans
```
ldapsearch -x -LLL -b 'dc=edt,dc=org' | grep dn 
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org
dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
dn: cn=Admin System,ou=usuaris,dc=edt,dc=org
```
