#!/bin/bash
# @edt ASIX M14-PROJECTE
# Descripció: programa per a ldap que executa fa diferents accions a la bases de dades depenent de l'argument donat a l'entrypoint
#		initdb → ho inicialitza tot de nou i fa el populate de edt.org.
#		slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades.
#		start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents.
#		slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada

case $1 in
	"initdb")
		rm -rf /var/lib/ldap/*
		rm -rf /etc/ldap/slapd.d/*
		slaptest -f slapd.conf -F /etc/ldap/slapd.d
		slapadd -F /etc/ldap/slapd.d -l edt-org.ldif
		chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
		/usr/sbin/slapd -d0;;
	"slapd")
		rm -rf /var/lib/ldap/*
		rm -rf /etc/ldap/slapd.d/*
		slaptest -f slapd.conf -F /etc/ldap/slapd.d
		chown -R openldap.openldap /etc/ldap/slapd.d/ /var/lib/ldap/
		/usr/sbin/slapd -d0;;
	"slapcat")
#		/usr/sbin/slapd -d0	
		if [ $# -eq 2 ] 
		then
			if [ $2 = "n1" ]
			then
				slapcat -n1 
			else
				slapcat -n0 
			fi
		else
			slapcat 
		fi;;
	"start"|"edtorg"|"")
		/usr/sbin/slapd -d0;;

esac
